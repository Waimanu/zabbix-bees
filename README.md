# zabbix-bees

[Zabbix](https://www.zabbix.com/) template and script for the
[btrfs](https://btrfs.wiki.kernel.org/index.php/Main_Page)
[bees](https://github.com/Zygo/bees) project

## Description

Monitoring [event-counters](https://github.com/Zygo/bees/blob/master/docs/event-counters.md)
items from the bees status file.

## Installation

- Deploy the UserParameter from [bees.conf](./bees.conf) file.
- Deploy the [bees.bash](./bees.bash) file to `/var/lib/zabbix-agent/` directory.
- Import the [zabbix-template-bees.yaml](./zabbix-template-bees.yaml)
  Template to your Zabbix.

## Zabbix Versions

See the following list of supported Operating systems with the Zabbix releases:

| Zabbix     | 5.4 |
|------------|-----|
| Arch Linux |  V  |
