#!/usr/bin/env bash

grep              \
  --no-filename   \
  --perl-regexp   \
  --only-matching \
  'UUID=\K.*' /etc/bees/*.conf          |
awk '{print "{\"{#UUID}\":\""$1"\"},"}' |
tr --delete '\n'                        |
sed 's/.$//'                            |
awk --field-separator '\n' '{print "{\"data\":["$1"]}"}'
